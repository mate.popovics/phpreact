const dev = process.env.NODE_ENV !== 'production';

export const server = dev ? process.env.NEXT_PUBLIC_NEXTJS_DEV_SERVER : process.env.NEXT_PUBLIC_NEXTJS_PROD_SERVER;
export const captcha_site_key = process.env.NEXT_PUBLIC_NEXTJS_CAPTCHA_SITE_KEY;

