import Layout from '../components/layout'
import {useCallback, useEffect, useState} from "react";
import Link from "next/link";
import {server} from "../config";

import { useContext } from 'react';
import UserContext from '../components/UserContext';

import {
    Link as SLink,
    Layout as SLayout,
    Card,
    TextContainer,
} from '@shopify/polaris';

export default function Home() {

    const { cookieTokenHandler, logoutHandler, getPageResponse } = useContext(UserContext);

    const [stylesLinkLoggedInUser, setStylesLinkLoggedInUser] = useState({display: 'none'})
    const [stylesLinkEditor, setStylesLinkEditor] = useState({display: 'none'})

    const [outputUserName, setOutputUserName] = useState('')
    const [outputLastLogin, setOutputLastLogin] = useState('')
    const [outputRoles, setOutputRoles] = useState('')


    useEffect(() => {
        if (!cookieTokenHandler()) return;

        getPageResponse(server + '/api/user', 'get').then((response) => {
            if (response.error) {
                alert(response.message)
                return
            }

            if (response.lastLogin) {
                const lastLogin = new Date(response.lastLogin);
                setOutputLastLogin(lastLogin.toLocaleString())
            }

            setOutputUserName(response.userName)
            setOutputRoles(response.roles.join(', '))

            if (response.authorizedPages.includes('loggedinuser')) {
                setStylesLinkLoggedInUser({display: 'block'})
            }

            if (response.authorizedPages.includes('editor')) {
                setStylesLinkEditor({display: 'block'})
            }
        })
    }, []);

    return (
     <Layout noHomeLink>
        <SLayout.Section>
            <Card title="User dashboard" sectioned>
                <TextContainer>Information below came from backend side with authenticated JWT token</TextContainer>
                <Card.Section title="User information">
                    <Card.Subsection>
                        <div><b>Username:</b> {outputUserName}</div>
                        <div><b>Last login:</b> {outputLastLogin}</div>
                        <div><b>Roles:</b> {outputRoles}</div>
                    </Card.Subsection>
                </Card.Section>

                <Card.Section title="Granted pages">
                    <div style={stylesLinkLoggedInUser}>
                        <Link href="/loggedinuser">
                            <a>Page of Logged In Users</a>
                        </Link>
                    </div>

                    <div style={stylesLinkEditor}>
                        <Link href="/editor" >
                            <a>Page of Editors</a>
                        </Link>
                    </div>
                </Card.Section>

                <Card.Section title="Log out">
                    <SLink onClick={logoutHandler}>Log Out</SLink>
                </Card.Section>

          </Card>
      </SLayout.Section>
     </Layout>
  )
}

