import Layout, {siteTitle} from '../components/layout'
import {server} from "../config";
import {useContext, useEffect, useState} from "react";
import UserContext from "../components/UserContext";

import {
    Layout as SLayout,
    Card,
    TextContainer,
} from '@shopify/polaris';

export default function LoggedInPage() {

    const { cookieTokenHandler, getPageResponse } = useContext(UserContext);

    const [pageResponse, setPageResponse] = useState('')

    useEffect(() => {
        if (!cookieTokenHandler()) return;
        getPageResponse(server + '/api/page/loggedinuser', 'get').then((response) => {
            if (response.error) {
                alert(response.message)
            } else {
                setPageResponse(response.content)
            }
        })
    }, []);

    return (
        <Layout>
            <SLayout.Section>
                <Card title="Logged in user's page" sectioned>
                    <TextContainer>Information below came from backend side with authenticated JWT token</TextContainer>
                    <Card.Section title="Content">
                        <TextContainer>{pageResponse}</TextContainer>
                    </Card.Section>
                </Card>
            </SLayout.Section>
        </Layout>
    )

}

