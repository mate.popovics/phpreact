import UserContext from "../components/UserContext";
import {useEffect, useState, useCallback} from "react";
import {useCookies} from "react-cookie";
import Router from "next/router";
import {AppProvider} from '@shopify/polaris';
import enTranslations from '@shopify/polaris/locales/en.json';
import '@shopify/polaris/dist/styles.css';

import {
    Toast
} from '@shopify/polaris';

export default function App({ Component, pageProps }) {

    const [cookies, setCookie, removeCookie] = useCookies(['token', 'authPages']);

    const cookieTokenHandler = () => {
        if (!cookies.token || !cookies.authPages) {
            Router.push('/login')
            return false
        }
        return true
    }

    const logoutHandler = () => {
        removeCookie('token')
        removeCookie('authPages')
        Router.push('/login')
    }

    const getPageResponse = async (url, method) => {
        const headers = new Headers({
            'Content-Type': 'application/json',
            'Authorization': cookies.token
        })

        try {
            const response = await fetch(url, {
                headers,
                method,
            })

            if (response.status === 403 || response.status === 401) {
                logoutHandler()
                //toggleToastActive()
                return {
                    "error": true,
                    "message": "Authentication error!"
                }
            }

            return await response.json()
        } catch (e) {
            return {
                "error": true,
                "message": "Unexpected error!"
            }
        }
    };

    const [toastActive, setToastActive] = useState(false);
    const toggleToastActive = useCallback(() => setToastActive((active) => !active), []);
    const toastMarkup = toastActive ? (
        <Toast content="Authentication error!" onDismiss={toggleToastActive} />
    ) : null;

    useEffect(() => {
        cookieTokenHandler()
    }, [cookies]);

    return (
    <UserContext.Provider value={{ cookieTokenHandler, logoutHandler, getPageResponse, toastMarkup }}>
        <AppProvider i18n={enTranslations}>
            <Component {...pageProps} />
        </AppProvider>
    </UserContext.Provider>
    )
}

