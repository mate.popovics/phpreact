import Layout from '../components/layout'
import { server, captcha_site_key } from '../config';
import React, {useState, useCallback, useEffect, useContext} from 'react'
import { useCookies } from 'react-cookie';
import Router from 'next/router'
import ReCAPTCHA from "react-google-recaptcha";
import {
    Layout as SLayout,
    Card,
    Button,
    FormLayout,
    TextField,
    TextContainer,
} from '@shopify/polaris';

export default function LoginPage() {

    const [cookies, setCookie] = useCookies(['token', 'authPages']);
    const [inputUserName, setInputUserName] = useState('')
    const [inputPassword, setInputPassword] = useState('')

    const [needCaptcha, setNeedCaptcha] = useState(false)

    const recaptchaRef = React.createRef();

    const handleInputUserNameChange = useCallback((newValue) => setInputUserName(newValue), []);
    const handleInputPasswordChange = useCallback((newValue) => setInputPassword(newValue), []);

    const handleSubmit = useCallback(() => {
            let recaptchaValue = ''
            if (needCaptcha) {
                recaptchaValue = recaptchaRef.current.getValue();
            }

            getLoginResponse(inputPassword, inputUserName, recaptchaValue).then((response) => {
                if (response.needCaptcha) {
                    setNeedCaptcha(true)
                } else {
                    setNeedCaptcha(false)
                }
                if (response.token) {
                    setCookie('token', response.token, { path: '/' });
                    setCookie('authPages', response.user.authorizedPages, { path: '/' });
                    Router.push('/')
                }
            })
        },
        [inputPassword, inputUserName]
    );

    const captchaElement = needCaptcha
        ? (<ReCAPTCHA
            ref={recaptchaRef}
            sitekey={captcha_site_key}
        />)
        : ''

    return (
        <Layout noHomeLink>
            <SLayout.Section>
                <Card title="Login Page" sectioned>
                    <Card.Section title={"form"}>
                        <Card.Subsection>
                            <TextContainer>User login page with JWT token authentication</TextContainer>
                        </Card.Subsection>
                        <Card.Subsection>
                            <FormLayout>
                                <TextField type="text" value={inputUserName} label="Username" onChange={handleInputUserNameChange} />
                                <TextField type="password" value={inputPassword} label="Password" onChange={handleInputPasswordChange} />

                                <div>{captchaElement}</div>

                                <Button submit onClick={handleSubmit}>Submit</Button>
                            </FormLayout>
                        </Card.Subsection>
                    </Card.Section>
                </Card>
            </SLayout.Section>
        </Layout>
    )
}

export async function getLoginResponse(inputPassword, inputUserName, recaptchaValue) {

    const headers = new Headers({
        'Content-Type': 'application/json',
    })
    const method = 'post'
    const body = JSON.stringify({
        "password": inputPassword,
        "userName": inputUserName,
        "recaptchaValue": recaptchaValue
    })

    try {
        const response = await fetch(server + '/api/auth/login', {
            headers,
            method,
            body
        })

        if (response.status === 401) {
            alert('Authentication error!')
        }
        return  await response.json()
    } catch (e) {
        alert('Unexpected error happened during login request!');
        return '';
    }
}