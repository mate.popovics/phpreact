import Head from 'next/head'
import styles from './layout.module.css'
import Link from 'next/link'

const name = 'Test application'
export const siteTitle = 'Test application'
import UserContext from "../components/UserContext";

import {
    Layout as SLayout,
    Page,
    Frame
} from '@shopify/polaris';
import React, {useContext} from "react";

export default function Layout({ children, noHomeLink }) {

    const { toastMarkup } = useContext(UserContext);

    return (

        <Page title={siteTitle}>
            <Frame>
                <SLayout>

                <Head>
                    <title>{siteTitle}</title>
                    <link rel="icon" href="/favicon.ico" />
                    <meta
                        name="description"
                        content="Test application based on Symfony5 and Next.js"
                    />
                </Head>

                {children}
                    {!noHomeLink && (
                        <SLayout.Section>
                            <div className={styles.backToHome}>
                                <Link href="/">
                                    <a>← Back to home</a>
                                </Link>
                            </div>
                        </SLayout.Section>
                )}
                {toastMarkup}
                </SLayout>
            </Frame>
        </Page>
    )
}