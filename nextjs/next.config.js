module.exports = {
    async rewrites() {
        return [
            {
                source: '/api/',
                destination: 'http://localhost/api/',
            },
        ]
    },
}