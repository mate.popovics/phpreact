<?php


namespace App\Provider;


use App\Entity\User;
use App\Enum\AuthorizePageMapEnum;

class AuthorizedPageListProvider
{

    /**
     * @param User $user
     * @return array
     */
    public function providePageList(User $user): array
    {
        $userRoles = $user->getRoles();
        return $this->getAuthorizedPagesByRoles($userRoles);

    }

    /**
     * @param array $roles
     * @return array
     */
    private function getAuthorizedPagesByRoles(array $roles): array
    {
        $pages = [];
        foreach ($roles as $role) {
            $pages = array_merge($pages, $this->getAuthorizedPagesByRole($role));
        }
        return array_unique($pages);
    }

    /**
     * @param string $role
     * @return array
     */
    private function getAuthorizedPagesByRole(string $role): array
    {
        $pages = [];
        foreach (AuthorizePageMapEnum::getMapByPages() as $page => $roles) {
            if (in_array($role, $roles)) {
                $pages[] = $page;
            }
        }
        return $pages;
    }


}