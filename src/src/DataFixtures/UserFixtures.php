<?php


namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUser($manager, 'Admin', 'admin@example.com', ['ROLE_ADMIN']);
        $this->loadUser($manager, 'User 1', 'user1@example.com', ['ROLE_EDITOR', 'ROLE_LOGGED_IN']);
        $this->loadUser($manager, 'User 2', 'user2@example.com', ['ROLE_EDITOR']);
        $this->loadUser($manager, 'User 3', 'user3@example.com', ['ROLE_LOGGED_IN']);
    }

    /**
     * @param ObjectManager $manager
     * @param string $userName
     * @param string $email
     * @param array $roles
     */
    private function loadUser(ObjectManager $manager, string $userName, string $email, array $roles): void {
        $user = new User();
        $user->setUserName($userName);
        $user->setEmail($email);
        $user->setRoles($roles);
        $user->setPassword($this->encoder->encodePassword($user, '12345'));

        $manager->persist($user);
        $manager->flush();
    }

}
