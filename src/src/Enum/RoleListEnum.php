<?php


namespace App\Enum;


class RoleListEnum
{

    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_EDITOR = 'ROLE_EDITOR';
    public const ROLE_LOGGED_IN = 'ROLE_LOGGED_IN';
    public const ROLE_USER = 'ROLE_USER';

    /**
     * @return string[]
     */
    public static function getRoles(): array {
        return [
            self::ROLE_ADMIN,
            self::ROLE_EDITOR,
            self::ROLE_LOGGED_IN,
            self::ROLE_USER
        ];
    }

    /**
     * @return string[]
     */
    public static function getDefaultRoles(): array {
        return [
            self::ROLE_USER
        ];
    }


}