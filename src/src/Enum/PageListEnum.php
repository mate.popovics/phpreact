<?php


namespace App\Enum;


class PageListEnum
{

    public const PAGE_EDITOR = 'editor';
    public const PAGE_LOGGED_IN = 'loggedinuser';

    /**
     * @return string[]
     */
    public static function getPages(): array
    {
        return [
            self::PAGE_EDITOR,
            self::PAGE_LOGGED_IN
        ];
    }

}