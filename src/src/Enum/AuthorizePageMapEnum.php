<?php


namespace App\Enum;


class AuthorizePageMapEnum
{

    static public function getMapByPages(): array
    {
        return [
            PageListEnum::PAGE_LOGGED_IN => [
                RoleListEnum::ROLE_LOGGED_IN,
                RoleListEnum::ROLE_ADMIN
            ],
            PageListEnum::PAGE_EDITOR => [
                RoleListEnum::ROLE_EDITOR,
                RoleListEnum::ROLE_ADMIN
            ]
        ];
    }

}