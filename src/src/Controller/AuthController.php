<?php

namespace App\Controller;

use App\Handler\LoginRequestHandler;
use App\Handler\RegisterRequestHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthController extends AbstractController
{

    /**
     * @Route("/api/auth/register", name="register", methods={"POST"})
     *
     * @param Request $request
     * @param RegisterRequestHandler $requestHandler
     * @return JsonResponse
     */
    public function register(Request $request, RegisterRequestHandler $requestHandler): Response
    {
        return $requestHandler->handle($request);
    }

    /**
     * @Route("/api/auth/login", name="login", methods={"POST"})
     *
     * @param Request $request
     * @param LoginRequestHandler $requestHandler
     * @return JsonResponse
     */
    public function login(Request $request, LoginRequestHandler $requestHandler): Response
    {
        return $requestHandler->handle($request);
    }

}
