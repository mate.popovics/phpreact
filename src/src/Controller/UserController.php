<?php

namespace App\Controller;

use App\Entity\User;
use App\Provider\AuthorizedPageListProvider;
use Firebase\JWT\JWT;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;

class UserController extends AbstractController
{
    /**
     * @Route("/api/user", name="user", methods={"GET"})
     * @param Request $request
     * @param UserRepository $userRepository
     * @param AuthorizedPageListProvider $pageListProvider
     * @return JsonResponse
     */
    public function index(Request $request, UserRepository $userRepository, AuthorizedPageListProvider $pageListProvider): JsonResponse
    {

        $user = $this->getUserByToken($request, $userRepository);

        $data = [
            "userName" => $user->getUsername(),
            "lastLogin" => $user->getLastLogin(),
            "authorizedPages" => $pageListProvider->providePageList($user),
            "roles" => $user->getRoles()
        ];
        return $this->json($data);
    }

    /**
     * @param Request $request
     * @param UserRepository $userRepository
     * @return User
     */
    private function getUserByToken(Request $request, UserRepository $userRepository): User {
        $jwtToken = str_replace('Bearer ', '', $request->headers->get('authorization'));
        $decoded = JWT::decode($jwtToken, $this->getParameter('jwt_secret'), array('HS256'));

        /** @var User $user */
        $user = $userRepository->findOneBy([
            'userName'=>$decoded->user,
        ]);
        return $user;
    }

}