<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{

    /**
     * @Route("/api/page/{pageName}", name="page", methods={"GET"}, requirements={"pageName"="^(editor|loggedinuser)$"})
     *
     * @param string $pageName
     * @return JsonResponse
     */
    public function index(string $pageName): Response
    {
        $data = [
            "content" => sprintf('This is the content of page "%s"!', $pageName)
        ];
        return $this->json($data);
    }

}