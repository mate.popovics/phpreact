<?php

namespace App\Handler;


use App\Entity\User;
use App\Provider\AuthorizedPageListProvider;
use Doctrine\ORM\EntityManagerInterface;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class LoginRequestHandler extends AbstractRequestHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var AuthorizedPageListProvider
     */
    private $pageListProvider;

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var int
     */
    private $captchaNeedThreshold;

    /**
     * @var string
     */
    private $captchaUrl;

    /**
     * @var string
     */
    private $captchaSecret;

    /**
     * @var string
     */
    private $jwtSecret;

    /**
     * LoginRequestHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $encoder
     * @param AuthorizedPageListProvider $pageListProvider
     * @param HttpClientInterface $client
     * @param int $captchaNeedThreshold
     * @param string $captchaUrl
     * @param string $captchaSecret
     * @param string $jwtSecret
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder, AuthorizedPageListProvider $pageListProvider, HttpClientInterface $client, int $captchaNeedThreshold, string $captchaUrl, string $captchaSecret, string $jwtSecret)
    {
        $this->entityManager = $entityManager;
        $this->encoder = $encoder;
        $this->pageListProvider = $pageListProvider;
        $this->client = $client;
        $this->captchaNeedThreshold = $captchaNeedThreshold;
        $this->captchaUrl = $captchaUrl;
        $this->captchaSecret = $captchaSecret;
        $this->jwtSecret = $jwtSecret;
    }


    /**
     * {@inheritdoc}
     */
    public function handle(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUserByUserName($request->get('userName'));

        if (!$this->checkUserBasedConditions($request->get('password'), $user) || !$this->checkCaptchaConditions($user, $request->get('recaptchaValue'))) {
            return $this->handleAccessDenied($user);
        }

        $jwt = $this->getEncodedJWT($user);

        $this->setLastLoginAndTries($user);

        return $this->getResponse([
            'user' => [
                'userName' => $user->getUsername(),
                'roles' => $user->getRoles(),
                'lastLogin' => $user->getLastLogin(),
                'authorizedPages' => $this->pageListProvider->providePageList($user)
            ],
            'token' => sprintf('Bearer %s', $jwt),
            'needCaptcha' => false
        ], Response::HTTP_OK);

    }

    /**
     * @param User $user
     */
    private function setLastLoginAndTries(User $user): void {
        $user->setLastLogin(new \DateTime());
        $user->setTries(0);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param User|null $user
     * @return Response
     */
    private function handleAccessDenied(?User $user): Response
    {
        if ($user) {
            $user->setTries($user->getTries() + 1);
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return $this->getResponse([
            'success' => false,
            'needCaptcha' => $user && $this->captchaNeedThreshold <= $user->getTries()
        ], Response::HTTP_UNAUTHORIZED);

    }

    /**
     * @param string $userName
     * @return User|null
     */
    private function getUserByUserName(string $userName): ?User {
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'userName'=>$userName,
        ]);
        return $user;
    }

    /**
     * @param $password
     * @param User|null $user
     * @return bool
     */
    private function checkUserBasedConditions($password, ?User $user = null): bool {
        if (!$user || !$this->encoder->isPasswordValid($user, $password)) {
            return false;
        }
        return true;
    }

    /**
     * @param User $user
     * @param string $captchaValue
     * @return bool
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function checkCaptchaConditions(User $user, string $captchaValue): bool {
        if ($user->getTries() >= $this->captchaNeedThreshold) {
            if (!$captchaValue) {
                return false;
            }

            $response = $this->client->request('POST', $this->captchaUrl, [
                'body' => [
                    'response' => $captchaValue,
                    'secret' => $this->captchaSecret
                ]]);
            $content = $response->toArray();
            if ($content['success'] !== true) {
               return false;
            }
        }
        return true;
    }

    /**
     * @param User $user
     * @return string
     */
    private function getEncodedJWT(User $user): string {
        $payload = [
            "user" => $user->getUsername(),
            "exp"  => (new \DateTime())->modify("+60 minutes")->getTimestamp(),
        ];

        return JWT::encode($payload, $this->jwtSecret, 'HS256');
    }

}