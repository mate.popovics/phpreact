<?php

namespace App\Handler;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface RequestHandlerInterface
{

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response;
}