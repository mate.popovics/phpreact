<?php

namespace App\Handler;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractRequestHandler implements RequestHandlerInterface
{

    /**
     * @param array $data
     * @param int|null $statusCode
     * @return Response
     */
    protected function getResponse(array $data, ?int $statusCode = NULL): Response {
        $response = new JsonResponse($data);

        if ($statusCode) {
            $response->setStatusCode($statusCode);
        }
        return $response;
    }
}