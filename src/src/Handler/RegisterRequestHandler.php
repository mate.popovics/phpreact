<?php

namespace App\Handler;


use App\Entity\User;
use App\Enum\RoleListEnum;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterRequestHandler extends AbstractRequestHandler
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * RegisterHandler constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $manager
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $manager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->manager = $manager;
    }


    /**
     * {@inheritdoc}
     */
    public function handle(Request $request): Response {
        $user = $this->createUserByRequest($request);
        $this->manager->persist($user);
        $this->manager->flush();

        return $this->getResponse([
            'user' => $user->getUsername()
        ]);
    }

    /**
     * @param Request $request
     * @return User
     */
    private function createUserByRequest(Request $request): User {
        $user = new User();
        $user->setUserName($request->get('username'));
        $user->setPassword($this->passwordEncoder->encodePassword($user, $request->get('password')));
        $user->setEmail($request->get('email'));
        $user->setRoles(RoleListEnum::getDefaultRoles());
        return $user;
    }
}